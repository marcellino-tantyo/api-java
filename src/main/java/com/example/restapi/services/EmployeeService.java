package com.example.restapi.services;

import com.example.restapi.exception.Exception;
import com.example.restapi.model.entity.Employee;
import com.example.restapi.model.repository.EmployeeRepository;
import com.example.restapi.model.result.SimpleResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public SimpleResult save(Employee employee) {
        try {
            if (employee.getGrade() >= 1 && employee.getGrade() <= 3) {
                Employee emp = employeeRepository.save(employee);
                return new SimpleResult("Success Insert", emp);
            }
            return new SimpleResult("Failed grade must between 1 and 3", employee);
        } catch (Exception e) {
            return new SimpleResult("Error, " + e, employee);
        }
    }

    public SimpleResult edit(Long id, Employee employeeParam) {

        try {
            Employee employee = employeeRepository.findById(id)
                    .orElseThrow(() -> new Exception("Employee", "id", id));

            if (!(employee.getGrade() >= 1 && employee.getGrade() <= 3)) {
                return new SimpleResult("Failed grade must between 1 and 3", employee);
            }

            employee.setName(employeeParam.getName() != null ? employeeParam.getName() : employee.getName());
            employee.setSalary(employeeParam.getSalary() != null ? employeeParam.getSalary() : employee.getSalary());
            employee.setGrade(employeeParam.getGrade() != null ? employeeParam.getGrade() : employee.getGrade());

            employeeRepository.save(employee);
            return new SimpleResult("Success updated", employee);
        } catch (Exception e) {
            return new SimpleResult("Error, " + e, employeeParam);
        }


    }

    public Iterable<Employee> findAllTransaction() {
        try {
            Iterable<Employee> employees = employeeRepository.findAll();
            List<Employee> employeeList = new ArrayList<>();
            for (Employee e: employees
            ) {
                double bonus;
                if (e.getGrade() == 1) {
                    bonus = 0.1;
                } else if (e.getGrade() == 2) {
                    bonus = 0.06;
                } else {
                    bonus = 0.03;
                }

                e.setTotalBonus((long)(bonus * e.getSalary()) + e.getSalary());

                employeeList.add(e);
            }
            return employeeList;
        } catch (Exception e) {
            return null;
        }
    }
}
