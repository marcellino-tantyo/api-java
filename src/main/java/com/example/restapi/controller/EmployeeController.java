package com.example.restapi.controller;

import com.example.restapi.model.entity.Employee;
import com.example.restapi.model.result.SimpleResult;
import com.example.restapi.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    public SimpleResult create(@RequestBody Employee employee) {
        return employeeService.save(employee);
    }

    @PutMapping("/{id}")
    public SimpleResult update(
            @PathVariable(value = "id") Long id,
            @RequestBody Employee employee) {
        return employeeService.edit(id, employee);
    }

    @GetMapping("/getAllTransaction")
    public Iterable<Employee> findAll() {
        return employeeService.findAllTransaction();
    }
}
