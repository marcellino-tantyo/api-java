package com.example.restapi.model.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "salary")
    private Long salary;

    @Column(name = "grade")
    private Integer grade;

    private Long totalBonus;

    protected Employee() {
    }

    public Employee(Long id, String name, Long salary, Integer grade) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getTotalBonus() {
        return totalBonus;
    }

    public void setTotalBonus(Long totalBonus) {
        this.totalBonus = totalBonus;
    }
}
